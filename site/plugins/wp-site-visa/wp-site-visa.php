<?php

/**
 * Plugin Name: Wp-Site-Visa
 * Plugin URI: 
 * Description: Wp-Site-Visa
 * Author: Agile Solutions PK
 * Author URI:
 * Version: 1.0
 */
 if ( !class_exists('Agile_wp_site_visa')){
	class Agile_wp_site_visa{
		function __construct(){
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('admin_menu', array(&$this,'admin_menu_page'));
			add_action( 'wp_ajax_aspk_check_email',  array(&$this,'aspk_check_email_exist' ));
			add_action( 'wp_ajax_nopriv_aspk_check_email', array(&$this,'aspk_check_email_exist' ));
			add_action( 'wp_ajax_aspk_forget',  array(&$this,'aspk_handle_forgot' ));
			add_action( 'wp_ajax_nopriv_aspk_forget', array(&$this,'aspk_handle_forgot' ));
			add_action( 'wp_ajax_aspk_sign_now',  array(&$this,'aspk_handle_signin' ));
			add_action( 'wp_ajax_nopriv_aspk_sign_now', array(&$this,'aspk_handle_signin' ));
			add_action( 'wp_ajax_aspk_verify_user',  array(&$this,'aspk_verify_email_all_users' ));
			add_action( 'wp_ajax_nopriv_aspk_verify_user', array(&$this,'aspk_verify_email_all_users' ));
			add_action( 'wp_ajax_aspk_set_pagess',  array(&$this,'aspk_save_settings' ));
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			add_action('wp_enqueue_scripts', array(&$this, 'frontend_scripts') );
			add_action('admin_menu', array(&$this,'admin_menu_options_page'));
			add_action( 'init', array(&$this, 'fe_init') );
			add_action('wp_footer', array(&$this,'footer_init' ));
			add_action('get_header', array(&$this,'aspk_header' ));
		}
		
		function fe_init(){
			ob_start();
		}
		
		function install(){
			global $wpdb;
			
			$sql="
				   CREATE TABLE `".$wpdb->prefix."aspk_email_verification_table` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `dt` bigint(20) NOT NULL,
				  `email` varchar(255) NOT NULL,
				  `random_num` varchar(255) NOT NULL,
				  `user_name` varchar(255) NOT NULL,
				  `user_password` varchar(255) NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=latin1;";
			$wpdb->query($sql);
		}
		
		function aspk_verify_email_all_users(){
			global $wpdb;
			
			$link_id = $_GET['link_id'];
			$aspk_hash = $_GET['aspk_hash'];
			if($link_id && $aspk_hash){
				$sql = "select * from {$wpdb->prefix}aspk_email_verification_table where id = '{$link_id}' AND random_num = '{$aspk_hash}'";
				$rs = $wpdb->get_row($sql);
				$user_name = $rs->user_name;
				$password = $rs->user_password;
				$email = $rs->email;
				if($rs){
					$user_id = wp_create_user( $user_name, $password, $email );
					if(is_numeric($user_id)){
						wp_set_auth_cookie($user_id);
						?>
						<script>
							window.location.href="<?php echo home_url() ; ?>"
						</script>
						<?php
						exit;
					}else{
						echo $user_id->get_error_message();
						exit;
					} 
				}
			}
			exit;
		}
		
		function aspk_show_login_screen_html(){
			/* global $wpdb;
			
			if(isset($_POST['aspk_signup'])){
				$email = $_POST['email-adr'];
				$password = $_POST['pwd'];
				$user_email = explode("@",$email);
				$user_name = $user_email[0];
				$random_number = $this->aspk_random_number();
				$date = time();
				$diff_time = 172800;
				$last_two_days = $date - $diff_time;
				$sql = "INSERT INTO {$wpdb->prefix}aspk_email_verification_table(dt,email,random_num,user_name,user_password) VALUES('{$date}','{$email}','{$random_number}','{$user_name}','{$password}')";
				$wpdb->query($sql);
				$inserted_id = $wpdb->insert_id;
				
				$sql2 = "DELETE from `{$wpdb->prefix}aspk_email_verification_table` where dt <= '{$last_two_days}' ";
				$wpdb->query($sql2);
				
				$verify_url = admin_url('admin-ajax.php?action=aspk_verify_user&link_id='.$inserted_id.'&aspk_hash='.$random_number);
				$link = "<a href = ".$verify_url.">Click here to complete your registration</a>";
				
				//$link_mail_genrate = file_get_contents(__DIR__.'/email-wp-site-visa.php'); 
				$link_mail_genrate =  get_option( 'aspk_email_html_confirm' ); 
				$link_mail_genrate = str_replace( '**username**' ,$user_name, $link_mail_genrate );
				$link_mail_genrate = str_replace( '**Click here to complete your registration**' ,$link, $link_mail_genrate );
				add_filter( 'wp_mail_content_type',array(&$this,'wp_set_content_type' ));
				$send_mail = wp_mail( $email, 'Confirmation mail', $link_mail_genrate );
				remove_filter( 'wp_mail_content_type', array(&$this,'wp_set_content_type' ));
				?>
				<!--<div class="email_confrm_message">
					<p>Registration email sent to <?php echo $email; ?> Open this email to finish signup.</p>
					<p>If you don't see this email in your inbox within 15 minutes, look for it in your junk mail folder. <br/>If you find it there, please mark the email as "Not Junk"</p>
				</div>-->
				<?php
				
			} */
			?>
			<div id="aspk_freeze" class="freeze"></div>
			<div  style="position: absolute; left: 50%;">
				<div id="aspk_login_box">
					<div class="email_confrm_message" id="aspk_show_registration_msg" style="display:none;">
						<p>Registration email sent to your email Open your email to finish signup.</p>
						<p>If you don't see this email in your inbox within 15 minutes, look for it in your junk mail folder. <br/>If you find it there, please mark the email as "Not Junk"</p>
					</div>
					<form  method="post" action="" id="aspk_signup_form" style="display:none;">
						<div style="padding: 41px 37px 23px 37px;">
							<div  class="heading-popup">
								<h3 class="aspk_heading-popup" style="font-size:27px;text-align:center;font-family:'DidotLTStdItalic13774';color:#fff;">
									Welcome back
								</h3>
							</div>
							<div  class="2nd-heading-popup">
								<h2 style="font-size: 33px;text-align: center;text-transform: uppercase !important;font-family: 'FuturaStdBold';color: #ffffff;letter-spacing: 2px;padding: 5px 0 0;line-height: 6px;">sign up</h2>
								<p style="margin: 0;font-family: 'FuturaStdMedium';font-size: 14px;color:#fff;text-align:center;text-transform: uppercase;padding-bottom: 15px;padding-top: 15px;"></p>
								
							</div>
								<div style="clear:both;text-align:center">
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 17px;color: yellow;text-align: center;" id="aspk_msg"></div>
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 17px;color: yellow;text-align: center;" id="agile_singnup_fail">
									</div>
									<div style="clear:left;">
										<input required type="email" id="aspk_filed_email" placeholder="EMAIL ADDRESS" name="email-adr" class="aspk_span_filed" >
										<input required style="display:none;" class="aspk_span_filed" id="aspk_filed_pwd" type="password" placeholder="PASSWORD" name="pwd">
									</div>
									<div style="clear:left;">
										<input type="button" onclick="aspk_validtion_form();" value="join" name="submit_email" class="aspk_newsletter_submit" id="aspk_submit_email"> 
										<input style="display:none;" id="aspk_filed_signup" class="aspk_newsletter_submit"  type="submit" value="submit" name="aspk_signup">
									</div>
								</div>
								<div style="clear:both;display:none;border-top: 1px solid;color:white;" id="div_singin">
									<p style="margin: 0;font-family: 'DidotLTStdItalic13774';font-size: 18px;color:#fff;margin-top: 20px;text-align: center;" >Already a member? <a href="#" id="signin_link" style="font-size: 18px;color:#fff !important;padding: 0 0 0 4px;font-family: 'FuturaStdBold';" onclick="aspk_signin_click()">SIGN IN</p></a>
								</div>
						</div>
					</form> 
					<form  method="post" action="" id="aspk_signin_form">
						<div style="padding: 41px 37px 23px 37px;">
							<div  class="heading-popup">
								<h3 class="aspk_heading-popup" style="font-size:27px;text-align:center;font-family:'DidotLTStdItalic13774';color:#fff;">
									Welcome back
								</h3>
							</div>
							<div  class="2nd-heading-popup">
								<h2 style="font-size: 33px;text-align: center;text-transform: uppercase !important;font-family: 'FuturaStdBold';color: #ffffff;letter-spacing: 2px;padding: 10px 0 0;line-height: 6px;">sign IN</h2>
								<p style="margin: 0;font-family: 'FuturaStdMedium';font-size: 19px;color:#fff;text-align:center;padding: 10px;"></p>
								
							</div>
								<div style="clear:both;text-align:center">
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 14px;color: yellow;text-align: center;display:none;padding-bottom: 5px;" id="aspk_singin_email_msg">
										Email address/password incorrect. <br/>Please try again.
									</div>
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 14px;color: yellow;text-align: center;display:none;padding-bottom: 5px;" id="aspk_singin_email_msg_false">
										Please enter email and Password 
									</div>
									<div style="clear:left;">
										<input id="agile_email_field" type="email"  placeholder="EMAIL ADDRESS" name="email-adr" class="aspk_span_filed" >
									</div>
									<div style="clear:left;margin-top:1em;">
										<input id="agile_pasword" class="aspk_span_filed" id="aspk_filed_pwd" type="password" placeholder="PASSWORD" name="pwd">
									</div>
									<div style="clear:left;">
										<input  id="aspk_filed_signin" class="aspk_newsletter_submit" type="button" value="submit" onclick="agile_submit_sigin()" name="aspk_signin">
									</div>
									<div style="clear:left;">
										<a style="letter-spacing: 2px;text-transform: capitalize; color:white !important;font-family: 'DidotLTStdItalic13774';font-size:18px;"href="#" id="forgot" class="forgot_pass" onclick="forgot_pwd_click()">forgot password?</a>
									</div>
								</div>
								<div style="margin-top: 10px;clear:both;border-top: 1px solid;color:white;" id="div_singup">
									<p style="margin: 0;font-family: 'DidotLTStdItalic13774';font-size: 18px;color:#fff;margin-top: 20px;text-align: center;" >Not a member?<a href="#" style="font-size: 20px;color:#fff !important;padding: 0 0 0 4px;font-family: 'FuturaStdBold';" id="signin_link" onclick="aspk_signup_click()">SIGN UP</p></a>
								</div>
						</div>
					</form> 
					<form  method="post" action="" id="aspk_forget_psw_form" style="display:none;">
						<div style="padding: 41px 37px 23px 37px;">
							<div  class="heading-popup">
								<h3 class="aspk_heading-popup" style="font-size:27px;text-align:center;font-family:'DidotLTStdItalic13774';color:#fff;">
									Welcome back
								</h3>
							</div>
							<div  class="2nd-heading-popup">
								<h2 style="font-size: 33px;text-align: center;text-transform: uppercase !important;font-family: 'FuturaStdBold';color: #ffffff;letter-spacing: 2px;padding: 24px 0 0;margin: 0 0 5px;line-height: 46px;">forgot password</h2>
								
							</div>
								<div style="clear:both;text-align:center">
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 14px;color: yellow;text-align: center;display:none;" id="aspk_forget_msg">
											There is no user registered with this email address.
									</div>
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 14px;color: yellow;text-align: center;" id="aspk_forget_msg_fail"></div>
									<div style="clear:left;font-family: 'FuturaStdMedium';font-size: 14px;color: yellow;text-align: center;display:none;" id="aspk_forget_msg_true">
											Check your email for the new password.
									</div>
									<div style="clear:left;">
										<input id="aspk_f_email" type="email"  placeholder="EMAIL ADDRESS" name="email-adr" class="aspk_span_filed" value="">
									</div>
									<div style="clear:left;">
										<input  id="aspk_filed_signin_form" class="aspk_newsletter_submit" type="button"  value="submit" name="aspk_forget_form">
									</div>
								</div>
								<div style="margin-top: 10px;clear:both;border-top: 1px solid;color:white;" id="div_singup">
									<p style="margin: 0;font-family: 'DidotLTStdItalic13774';font-size: 18px;color:#fff;margin-top: 20px;text-align: center;" >Not a member?<a href="#" style="font-size: 20px;color:#fff !important;padding: 0 0 0 4px;font-family: 'FuturaStdBold';" id="signin_link" onclick="aspk_signup_click()">SIGN UP</p></a>
								</div>
						</div>
					</form> 
				</div>
			</div>
			<?php
		}
		
		function get_settings(){
			$defaults=array();
			$opt =get_option('aspk_add_page_option', $defaults);
			return $opt;
		}
		
		function get_settings_post(){
			$defaults=array();
			$opt =get_option('aspk_add_post_option', $defaults);
			return $opt;
		}
		
		function aspk_header(){
			global $post;
			
			if (! is_user_logged_in() ){
				if($post->post_type == 'page' ){
					$default_setting = $this->get_settings();
					$get_all_pages = get_option('aspk_add_page_option',$default_setting);
					if($get_all_pages){
						if(in_array($post->ID,$get_all_pages)){
								return;	
						}
					}
					$this->aspk_show_login_screen_html();
				}elseif($post->post_type == 'post' ){
					$default_setting_post = $this->get_settings_post();
					$get_all_posts = get_option('aspk_add_post_option',$default_setting_post);
					$post_categories = wp_get_post_categories( $post->ID );
					if($get_all_posts){
						foreach($get_all_posts as $p_id){
							if(in_array($p_id,$post_categories)){
									return;
									
							}
						}
					}
					$this->aspk_show_login_screen_html();
				}
			}
		}
		
		function footer_init(){
			$this->aspk_show_login_screen();	
		}
		
		function admin_menu_options_page(){
			add_options_page( 'Wp Site Visa', 'Wp Site Visa', 'manage_options', 'agile_page_setting',array( &$this , 'set_pages_to_show' ));
		}
		
		function aspk_save_settings(){
			$page_op = $_POST['page_val_name'];
			$post_op = $_POST['post_val_name'];
			update_option( 'aspk_add_page_option', $page_op );
			update_option( 'aspk_add_post_option', $post_op );
		
		
		}
		
		function set_pages_to_show(){
			?>
		<div class="tw-bs container" style="margin-top:1em;">
			<div style="border:1px solid;clear:left;padding:1em;float:left;background-color:floralwhite;width:60em;">
				<div class="row" style="clear:left;">
					<div class="col-md-12" style="margin-top:1em;">
						<h3>Please Select Pages and Posts that you don't want to Show</h3>
					</div>
				</div>
				<form method="post" action="">
					<div class="row">
						<div class="col-md-6">
							<div class="row" style="clear:left;">
									 <?php 
										$default_setting = $this->get_settings();
										$get_all_pages = get_option('aspk_add_page_option',$default_setting);
										?>
									<div class="col-md-5" style="margin-top:1em;"><b>Select Pages </b></div>
									<div class="col-md-7" style="margin-top:1em;">
									<?php echo $this->get_available_pages($get_all_pages); ?>
									</div>
							</div>
							<div class="row" style="clear:left;">
								<div class="col-md-7" style="margin-top:1em;"></div>
								<div class="col-md-5" style="margin-top:1em;">
									<input type="button" name="add" value="Add" class="btn btn-primary"  id="btn_add_pages" style="width:6em;">
								</div>
							</div>
							<div class="row" style="clear:left;">
								<?php 
									$default_setting_post = $this->get_settings_post();
									$get_posts = get_option('aspk_add_post_option',$default_setting_post);
								?>
								<div class="col-md-5" style="margin-top:1em;"><b>Select Post Category</b> </div>
								<div class="col-md-7" style="margin-top:1em;">	
									<?php echo  $this->get_available_posts($get_posts); ?>
								</div>
							</div>
							<div class="row" style="clear:left;">
								<div class="col-md-7" style="margin-top:1em;"></div>
								<div class="col-md-5" style="margin-top:1em;">
									<input type="button" name="add2" value="Add" class="btn btn-primary" id="btn_add_post" style="width:6em;">
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12" style="margin-top:1em;">
								<?php	$get_pages_all = get_option('aspk_add_page_option',true);
										
									?>
									<select id="show_select_pages" name="aspk_page[]" style="width:16em;" size="8">
									<?php 
										foreach($get_pages_all as $p_id){
											?><option value="<?php echo $p_id; ?>"><?php echo get_the_title( $p_id ); ?></option>
											<?php
										}
									?>
									</select>
								</div>
								<div class="row" style="clear:left;">
									<div class="col-md-2" style="margin-top:1em;"></div>
									<div class="col-md-10" style="margin-top:1em;">
										<input type="button" name="add" value="Remove" class="btn btn-primary" onclick="del_pages();" style="width:6em;">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12" style="margin-top:1em;">
								<?php	$post_id_all = get_option('aspk_add_post_option',true);
									
									?>
									<select required  id="show_select_posts" name="aspk_post[]" style="width:16em;" size="8">
										<?php 
										foreach($post_id_all as $pst_id){
												?><option value="<?php echo $pst_id; ?>"><?php echo get_cat_name( $pst_id  ); ?></option>
											<?php
											
										}
									?>
									</select>
								</div>
							</div>
							<div class="row" style="clear:left;">
								<div class="col-md-2" style="margin-top:1em;"></div>
								<div class="col-md-10" style="margin-top:1em;">
									<input type="button" name="add" value="Remove" class="btn btn-primary" onclick="del_posts();" style="width:6em;">
								</div>
							</div>
						</div>
					</div>
					<div id="dialog_box"></div>
					<div class="row" style="clear:left;">
						<div class="col-md-3"></div>
						<div class="col-md-9" id="aspk_show" style="margin-top:1em;display:none;color:red;">
							<h1>Please Save Settings</h1>
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-12" id="aspk_show_save_setting" style="margin:auto; text-align:center;display:none;color:green;">
							<h1>Your Settings has been Saved</h1>
						</div>
					</div>
					<div class="row" style="clear:left;">
						<div class="col-md-5" style="margin-top:1em;"></div>
						<div class="col-md-7" style="margin-top:1em;">
							<input type = "button" name = "set_page" class = "btn btn-primary" onclick="save_setting();"  value = "Save Settings"/> 
						</div>
					</div>
				</form>
			</div>
			<div style="border:1px solid;clear:left;padding:1em;float:left;background-color:floralwhite;margin-top:2em;width:60em;">
				<div class="row">
					<div class="col-md-12" style="margin-top:1em;">
						<h1 style="color: darkmagenta;">Forgot Email</h1>
					</div>
				</div>
				<?php 
					if(isset($_POST['aspk_email_submit'])){
						$email_html = $_POST['mycustomeditor'];
						update_option( 'aspk_email_html', $email_html );
					}
				?>
				<form method="post" action="">
					<div class="row">
						<div class="col-md-12" style="margin-top:1em;">
						<?php 
							$default_email = $this->show_default_template_html(); 
							$html_email = get_option( 'aspk_email_html',$default_email );
							$content = $html_email;
							$editor_id = 'mycustomeditor';
							$settings = array( 'media_buttons' => false );
							wp_editor( $content, $editor_id, $settings);
						?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-top:1em;">
							<input type="submit" name="aspk_email_submit" value="Save Email" class="btn btn-primary" id="btn_add_post" style="width:6em;">
						</div>
					</div>
				</form>
			</div>
			<div style="border:1px solid;clear:left;padding:1em;float:left;background-color:floralwhite;margin-top:2em;width:60em;">
				<div class="row">
					<div class="col-md-12" style="margin-top:1em;">
						<h1 style="color: darkmagenta;">Confirm Email</h1>
					</div>
				</div>
				<?php 
					if(isset($_POST['aspk_email_submit22'])){
						$email_html = $_POST['mycustomeditoremail2'];
						update_option( 'aspk_email_html_confirm', $email_html );
					}
				?>
				<form method="post" action="">
					<div class="row">
						<div class="col-md-12" style="margin-top:1em;">
						<?php 
							$default_email2 = $this->confirm_show_default_template_html(); 
							$html_email2 = get_option( 'aspk_email_html_confirm',$default_email2 );
							$content2 = $html_email2;
							$editor_id2 = 'mycustomeditoremail2';
							$settings2 = array( 'media_buttons' => false );
							wp_editor( $content2, $editor_id2, $settings2);
						?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" style="margin-top:1em;">
							<input type="submit" name="aspk_email_submit22" value="Save Email" class="btn btn-primary" id="btn_add_post" style="width:6em;">
						</div>
					</div>
				</form>
			</div>
			<script>
			jQuery(document).ready(function () {
				//hide a div after 3 seconds
				 setTimeout(function(){jQuery("#aspk_show").hide(); }, 3000);
			});
				setTimeout(function(){
				 jQuery("#aspk_show").hide(); }, 3000);
				 
				function del_pages(){
					if(jQuery('#show_select_pages option:selected').length < 1){
						jQuery( "#dialog_box" ).html('Please Select a Page');
						jQuery( "#dialog_box" ).dialog();
						return false;
					}
					var cval = jQuery('#show_select_pages').find(":selected").val();
					jQuery('#show_select_pages option[value="'+cval+'"]').remove();
					jQuery('#aspk_show_save_setting').hide();
					jQuery('#aspk_show').show();
				}
				function del_posts(){
					if(jQuery('#show_select_posts option:selected').length < 1){
						jQuery( "#dialog_box" ).html('Please Select a Category');
						jQuery( "#dialog_box" ).dialog();
						return false;
					}
					var cval = jQuery('#show_select_posts').find(":selected").val();
					jQuery('#show_select_posts option[value="'+cval+'"]').remove();
					jQuery('#aspk_show_save_setting').hide();
					jQuery('#aspk_show').show();
				}
					
				jQuery( "#btn_add_pages,#btn_add_post" ).on( "click", function() {
					if(this.id == 'btn_add_pages'){
						if(jQuery('#aspk_all_page_option option:selected').length < 1){
							jQuery( "#dialog_box" ).html('Please Select a Page');
							jQuery( "#dialog_box" ).dialog();
							return false;
						}
						jQuery('#aspk_all_page_option').find(":selected").each(function(index){
							var common = jQuery(this).val();
							var txt = jQuery(this).text();
							jQuery('#aspk_show').show();
							jQuery('#aspk_show_save_setting').hide();
							var added_common_field = jQuery("#show_select_pages option:contains('"+txt+"')").length;
							if(added_common_field == 1){
								jQuery( "#dialog_box" ).html('<p>Field '+txt+' Already Exists In Display Column</p>');
								jQuery( "#dialog_box" ).dialog();
								jQuery('#aspk_all_page_option').val('');
								return;
							}
							jQuery('#show_select_pages').append('<option value="'+common+'" >'+txt+'</option>');
						});
					}else if(this.id == 'btn_add_post'){
						if(jQuery('#aspk_all_post_option option:selected').length < 1){
							jQuery( "#dialog_box" ).html('Please Select a Category');
							jQuery( "#dialog_box" ).dialog();
							return false;
						}
						jQuery('#aspk_all_post_option').find(":selected").each(function(index){
							var val_post = jQuery(this).val();
							var post_txt = jQuery(this).text();
							jQuery('#aspk_show').show();
							jQuery('#aspk_show_save_setting').hide();
							var added_common_field = jQuery("#show_select_posts option:contains('"+post_txt+"')").length;
							if(added_common_field == 1){
								jQuery( "#dialog_box" ).html('<p>Field '+post_txt+' Already Exists In Display Column</p>');
								jQuery( "#dialog_box" ).dialog();
								jQuery('#aspk_all_post_option').val('');
								return;
							}
							jQuery('#show_select_posts').append('<option value="'+val_post+'" >'+post_txt+'</option>');
						});
					}
				});
				function save_setting(){
					var array_page = {};
					var array_post = {};
				
					jQuery('#aspk_show').hide();
					jQuery('#show_select_pages option').each(function(index){
							var val_page = jQuery(this).val();
							var page_txt = jQuery(this).text();
							array_page[index] = val_page;
							
					});
					jQuery('#show_select_posts option').each(function(index){
							var val_post = jQuery(this).val();
							var post_txt = jQuery(this).text();
							array_post[index] = val_post;
							
					});
					
					var data = {
						'action': 'aspk_set_pagess',
						'page_val_name': array_page ,     
						'post_val_name': array_post ,    
						  
					};
					var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
					jQuery.post(ajaxurl, data, function(response) {
						jQuery('#aspk_show_save_setting').show();
						
					});
				}
			</script>
		</div><!-- end container -->
			<?php
		}
		
		function confirm_show_default_template_html(){
			ob_start();
			?>
			<div style = "width:90%;">
					<h1 >My Site.com </h1><br/>
							Your username is: **username**<br/>
							
							**Click here to complete your registration**<br/>

							Thank you! 
			</div>
			<?php
			$html2 = ob_get_clean();
			return $html2;
		}
		
		function show_default_template_html(){
			ob_start();
			?>
			<div style = "width:90%;">
				<h1 >My Site.com </h1><br/>
					Your Username is: **username**<br/>
					Your Password is: **password**<br/><br>
					
					Thank you! 
			</div>
			<?php
			$html = ob_get_clean();
			return $html;
		}
		
		function get_available_posts($selected_posts = array()){
			//$selected_posts contains posts ids
			$args = array(
					'type'                     => 'post',
					'child_of'                 => 0,
					'parent'                   => '',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 1,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'taxonomy'                 => 'category',
					'pad_counts'               => false 

				); 
					$categories = get_categories( $args );
					?>
			<select id="aspk_all_post_option"  multiple size="8"  style="width:16em;">
			<?php

				foreach ( $categories as $category  ) {
					if(in_array($category ->term_id, $selected_posts)) continue;
					?>
					<option value="<?php echo $category ->term_id ; ?>"><?php echo $category ->name ; ?></option>
					<?php
				}
			  ?>
			</select>
			<?php
								
		}
		
		function get_available_pages($selected_pages = array()){
			//$selected_pages contains page ids
			$args = array(
						'sort_order' => 'DESC',
						'sort_column' => 'post_date',
						'hierarchical' => 1,
						'exclude' => '',
						'include' => '',
						'meta_key' => '',
						'meta_value' => '',
						'authors' => '',
						'child_of' => 0,
						'parent' => -1,
						'exclude_tree' => '',
						'number' => '',
						'offset' => 0,
						'post_type' => 'page',
						'post_status' => 'publish'
					);
			?>
			<select id="aspk_all_page_option"  multiple size="8"  style="width:16em;">
			<?php
				$pages = get_pages($args); 
				foreach ( $pages as $page ) {
					if(in_array($page->ID, $selected_pages)) continue;
					?>
					<option value="<?php echo $page->ID ; ?>"><?php echo $page->post_title ; ?></option>
					<?php
				}
			  ?>
			</select>
			<?php
		}
		
		function frontend_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-dialog','',array('jquery','jquery-ui-core'));
			wp_enqueue_script('js-wp-site-vise-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_style( 'agile-wp-site-vise-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
			wp_enqueue_style( 'aspk-wp-site-visa.css', plugins_url('css/wp-site-visa.css', __FILE__) );
			wp_enqueue_style('jquery-ui-css',"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css");
		}
		
		function wp_enqueue_scripts(){	
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui-dialog','',array('jquery','jquery-ui-core'));
			wp_enqueue_script('js-wp-site-vise-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_style( 'agile-wp-site-vise-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
			wp_enqueue_style( 'aspk-wp-site-visa.css', plugins_url('css/wp-site-visa.css', __FILE__) );
			wp_enqueue_style('jquery-ui-css',"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css");
		}
		
		function aspk_handle_signin(){
			$user_email = explode("@", $_POST['email-adr']);
			$creds = array();
			//$creds['user_login'] = $_POST['email-adr'];
			$creds['user_login'] = $user_email[0];
			$creds['user_password'] = $_POST['pwd'];
			$creds['remember'] = true;
			$user = wp_signon( $creds, false );
			if(is_wp_error( $user )){
				$ret = array('act' => 'sign_now','st'=>'fail','msg'=>$user->get_error_message());
				echo json_encode($ret);
				exit;
			}else{
				$ret = array('act' => 'sign_now','st'=>'ok','msg'=>'user_logged_in');
				echo json_encode($ret);
				exit;
			}
		}
		
		function random_password( $length = 8 ) { //generates random password.
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;?";
			$password = substr( str_shuffle( $chars ), 0, $length );
			return $password;
		}
		
		function aspk_random_number( $length = 8 ) { //generates random number.
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;?";
			$password = substr( str_shuffle( $chars ), 0, $length );
			return $password;
		}
		
		function wp_set_content_type(){
			return "text/html";
		}
		
		function aspk_handle_forgot(){
			$email = $_POST['email-adr'];
			$user = get_user_by( 'email', $email );
			if(empty($user)){
				$ret = array('act' => 'forgot_password','st'=>'fail','msg'=>'User Not Exist.');
				echo json_encode($ret);
				exit;
			}else{ 
				$user_id = $user->ID;
				$user_name = get_user_meta($user->ID,'nickname', true);
				$password = $this->random_password();
				$set_password = wp_set_password( $password, $user_id );
				$verify_url = home_url();
				$link = "<a href = ".$verify_url.">Click here to complete your registration</a>";
				//$link_mail_genrate = file_get_contents(__DIR__.'/email-wp-site-visa-forgot.php'); 
				$default_email = $this->show_default_template_html(); 
				$link_mail_genrate = get_option( 'aspk_email_html' ,$default_email); 
				$link_mail_genrate = str_replace( '**username**' ,$user_name, $link_mail_genrate );
				$link_mail_genrate = str_replace( '**password**' ,$password, $link_mail_genrate );
				$link_mail_genrate = str_replace( '**Click here to complete your registration**' ,$link, $link_mail_genrate );
				add_filter( 'wp_mail_content_type',array(&$this,'wp_set_content_type' ));
				$send_mail = wp_mail( $email, 'Reset Password', $link_mail_genrate );
				remove_filter( 'wp_mail_content_type', array(&$this,'wp_set_content_type' ));
				if($send_mail == true){
					$ret = array('act' => 'forgot_password','st'=>'ok','msg'=>'Mail Sent.');
					echo json_encode($ret);
					exit;
				}else{
					$ret = array('act' => 'forgot_password','st'=>'fail','msg'=>'Mail Not Sent.');
					echo json_encode($ret);
					exit;
				}
			}
		}
		
		function aspk_check_email_exist(){
			$email = $_POST['email-adr'];
			$check_user = get_user_by( 'email', $email );
			if($check_user == false){
				$ret = array('act' => 'check_user','st'=>'ok','msg'=>'user not exist');
				echo json_encode($ret);
			}else{
				$ret = array('act' => 'check_user','st'=>'fail','msg'=>'This email is already registered. Please access SIGN IN link below.');
				echo json_encode($ret);
			}
			exit;
		}
		
		function admin_menu_page(){
			//add_menu_page('Wp Site Visa ', 'Wp Site Visa ', 'manage_options', 'agile_wp_ite_visa', array(&$this, 'welcome_page') );
			
		}
		
		function welcome_page(){
			//$this->aspk_show_login_screen();
		}
		
		function aspk_show_login_screen(){
		global $wpdb;
			
			if(isset($_POST['aspk_signup'])){
				$email = $_POST['email-adr'];
				$password = $_POST['pwd'];
				$user_email = explode("@",$email);
				$user_name = $user_email[0];
				$random_number = $this->aspk_random_number();
				$date = time();
				$diff_time = 172800;
				$last_two_days = $date - $diff_time;
				$sql = "INSERT INTO {$wpdb->prefix}aspk_email_verification_table(dt,email,random_num,user_name,user_password) VALUES('{$date}','{$email}','{$random_number}','{$user_name}','{$password}')";
				$wpdb->query($sql);
				$inserted_id = $wpdb->insert_id;
				
				$sql2 = "DELETE from `{$wpdb->prefix}aspk_email_verification_table` where dt <= '{$last_two_days}' ";
				$wpdb->query($sql2);
				if($inserted_id && $random_number ){
				
					$verify_url = admin_url('admin-ajax.php?action=aspk_verify_user&link_id='.$inserted_id.'&aspk_hash='.$random_number);
					$link = "<a href = ".$verify_url.">Click here to complete your registration</a>";
				}
				//$link_mail_genrate = file_get_contents(__DIR__.'/email-wp-site-visa.php'); 
				$default_email2 = $this->confirm_show_default_template_html();
				$link_mail_genrate =  get_option( 'aspk_email_html_confirm',$default_email2 ); 
				$link_mail_genrate = str_replace( '**username**' ,$user_name, $link_mail_genrate );
				$link_mail_genrate = str_replace( '**Click here to complete your registration**' ,$link, $link_mail_genrate );
				add_filter( 'wp_mail_content_type',array(&$this,'wp_set_content_type' ));
				$send_mail = wp_mail( $email, 'Confirmation mail', $link_mail_genrate );
				remove_filter( 'wp_mail_content_type', array(&$this,'wp_set_content_type' ));
				?>
				<script>
					jQuery(document).ready(function(){
						jQuery('#aspk_signin_form').hide();
						jQuery('#aspk_forget_psw_form').hide();
						jQuery('#aspk_signup_form').hide();
						jQuery('#aspk_show_registration_msg').show();
					});
				</script>
				<?php
				
			}
			?>
		<div id="javascript_welcome">
			<script>
					function aspk_validtion_form(){
						var x = jQuery('#aspk_filed_email').val();
							var atpos = x.indexOf("@");
							var dotpos = x.lastIndexOf(".");
							if (atpos< 1 || dotpos<atpos+2 || dotpos+2>=x.length) {
								jQuery("#aspk_msg").html("Please enter valid email");
								return false;
							}
						jQuery("#aspk_msg").show();
						agile_check_email_exist();
					}
					function agile_check_email_exist(){
						jQuery("#agile_singnup_fail").html('');
						var aspk_email =   jQuery("#aspk_filed_email").val();
							var data = {
								'action': 'aspk_check_email',
								'email-adr': aspk_email      
							};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
							jQuery.post(ajaxurl, data, function(response) {
								jQuery("#aspk_fd_subcat").html(response);
								obj = JSON.parse(response);
								 if(obj.st == 'ok'){	
									jQuery("#aspk_msg").hide();
									jQuery("#agile_singnup_fail").html('');
									jQuery('#aspk_submit_email').hide();
									jQuery('#aspk_filed_signup').show();
									jQuery('#aspk_filed_pwd').show();
									jQuery('#aspk_filed_email').hide();
								 }
								 if(obj.st == 'fail'){
									 jQuery("#agile_singnup_fail").html(obj.msg);
									 jQuery('#aspk_filed_signup').hide();
									 jQuery('#aspk_filed_email').show();
									 jQuery('#aspk_filed_pwd').hide();
									 jQuery('#aspk_submit_email').show();
									 jQuery("#aspk_msg").hide();
								 }

							});
					}
					
					function aspk_signin_click(){
						jQuery("#agile_singnup_fail").html('');
						jQuery('#aspk_signin_form').show();
						jQuery('#aspk_signup_form').hide();
						jQuery('#div_singin').hide();
						jQuery('#aspk_forget_psw_form').hide();
						jQuery('#div_singup').show();
					}
					function aspk_signup_click(){
						jQuery("#agile_singnup_fail").html('');
						jQuery('#aspk_signin_form').hide();
						jQuery('#aspk_signup_form').show();
						jQuery('#div_singin').show();
						jQuery('#div_singup').hide();
						jQuery('#aspk_forget_psw_form').hide();
					}
					function forgot_pwd_click(){
						jQuery("#agile_singnup_fail").html('');
						jQuery('#aspk_signin_form').hide();
						//jQuery('#aspk_signup_form').hide();
						jQuery('#aspk_forget_psw_form').show();
					}
					jQuery("#aspk_filed_signin_form").on("click",function( event){
						var aspk_email =   jQuery("#aspk_f_email").val();
							if(aspk_email.length == 0){
								jQuery("#aspk_forget_msg_fail").html('Please enter email');
								event.preventDefault();
								return false;
							}
							var data = {
								'action': 'aspk_forget',
								'email-adr': aspk_email      
							};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
							jQuery.post(ajaxurl, data, function(response) {
								jQuery("#aspk_fd_subcat").html(response);
								obj = JSON.parse(response);
								 if(obj.msg == 'User Not Exist.'){
									 jQuery("#aspk_forget_msg").show();
									  jQuery("#aspk_forget_msg_true").hide();
									  jQuery("#aspk_forget_msg_fail").hide();
								 }
								 if(obj.msg == 'Mail Sent.'){
									 jQuery("#aspk_forget_msg_true").show();
									  jQuery("#aspk_forget_msg").hide();
									  jQuery("#aspk_forget_msg_fail").hide();
								 }

							});
					});
					function agile_submit_sigin(){
						jQuery("#aspk_singin_email_msg_false").hide();
						var aspk_email =   jQuery("#agile_email_field").val();
						var aspk_paswd =   jQuery("#agile_pasword").val();
							if(aspk_email.length == 0 && aspk_paswd.length == 0){
								jQuery("#aspk_singin_email_msg_false").show();
								event.preventDefault();
								return false;
							}
							var data = {
								'action': 'aspk_sign_now',
								'pwd': aspk_paswd ,
								'email-adr': aspk_email      
							};
							var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
							jQuery.post(ajaxurl, data, function(response) {
								jQuery("#aspk_fd_subcat").html(response);
								obj = JSON.parse(response);
								 if(obj.st == 'ok'){	
									window.location.href="<?php echo home_url() ; ?>"
								 }
								 if(obj.st == 'fail'){
									 jQuery("#aspk_singin_email_msg").show();
								 }

							});
					}
					
			</script>
		</div>
		<?php
		
		}	
	}// end class
}// end existing class
new Agile_wp_site_visa();